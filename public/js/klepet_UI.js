function divElementEnostavniTekst(sporocilo) {
  return replaceWithSmileys($('<div style="font-weight: bold"></div>'),sporocilo);
}

function uporabnikovoSporociloEnostavniTekst(sporocilo){
    sporocilo = filterSwearWords(sporocilo.split(" "));
    return replaceWithSmileys($('<div style="font-weight: bold"></div>'),sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    var kanal = $('#kanal').text().split("@");
    klepetApp.posljiSporocilo(kanal[kanal.length-1].substring(1), sporocilo);
    $('#sporocila').append(uporabnikovoSporociloEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>');
    if(sporocilo.sistemsko){
      novElement.text(sporocilo.besedilo)
    }
    else{
      novElement.text(sporocilo.avtor);
      var words = sporocilo.besedilo.split(" ");
      novElement = replaceWithSmileys(novElement,  filterSwearWords(words));
    }
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
  //Dodajanje Seznama uporabnikov
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
  
    for(var uporabnik in uporabniki) {
      if (uporabniki[uporabnik] != '') {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[uporabnik]));
      }
    }
  });
});


//Dodajanje podpore smeškom
function replaceWithSmileys(messageElement, message){
  
  for(var i = 0; i < message.length; i++){
    if(message.indexOf(";)",i)==i){
      i+=1;
      messageElement.append("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png' />");
    }
    else  if(message.indexOf(":)",i)==i){
      i+=1;
      messageElement.append("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png' />");
    }
    else  if(message.indexOf("(y)",i)==i){
      i+=2;
      messageElement.append("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png' />");
    }
    else  if(message.indexOf(":*",i)==i){
      i+=1;
      messageElement.append("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png' />");
    }
    else  if(message.indexOf(":(",i)==i){
      i+=1;
      messageElement.append("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png' />");
    }
    else{
      messageElement.append(document.createTextNode(message.charAt(i)));
    }
  }
  
  return messageElement;
}

//Dodajanje funkcionalnosti filtra
function filterSwearWords(message){
  for(var i = 0; i < swearWords.length; i++){
    for(var k= 0; k< message.length; k++){
      if(message[k].toLowerCase() == swearWords[i]){
          var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = output;
      }else if(message[k] == swearWords[i]+","){
        var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = output+",";
      }
      else if(message[k].toLowerCase() == swearWords[i]+"."){
        var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = output+".";
      }
      else if(message[k].toLowerCase() == swearWords[i]+"!"){
        var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = output+"!";
      }
      else if(message[k].toLowerCase() == swearWords[i]+"?"){
        var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = output+"?";
      }
      else if(message[k].toLowerCase() == swearWords[i]+"-"){
        var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = output+"-";
      }
       else if(message[k].toLowerCase() == "-"+swearWords[i]){
        var output = "";
          for(var u= 0; u< swearWords[i].length; u++){
            output+= "*";
          }
          message[k] = "-"+output;
      }
    }
    
  }
  return message.join(" ");
}

  
  
  


