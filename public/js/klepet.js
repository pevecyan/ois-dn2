var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanalZGeslom = function(kanal, geslo){
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
}

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      if(besede.join(' ').split("\"").length == 1){
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal);
      }else{
        var kanal = besede.join(' ').split("\"")[1];
        var geslo = besede.join(' ').split("\"")[3];
        this.spremeniKanalZGeslom(kanal, geslo);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      besede = besede.join(' ');
      besede = besede.split("\"");
      if(besede[3]){
        var podatki ={uporabnik:besede[1], sporocilo:besede[3]};
        
        var zasebno = $('<div style="font-weight: bold"></div>').text("(Zasebno sporočilo za "+podatki.uporabnik+"): ");
        zasebno = replaceWithSmileys(zasebno, filterSwearWords(podatki.sporocilo.split(" ")));
        $('#sporocila').append(zasebno);
        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
        
        
        
        this.socket.emit('zasebnoSporocilo', podatki);
      }else{
        sporocilo = 'Neznan ukaz.';
      }
      //Implementacija zasebnega sporočila
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};