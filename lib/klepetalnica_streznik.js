var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};

//Začetek implementacije sob z geslom
var geslaZaKanale = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnaSporocila(socket, vzdevkiGledeNaSocket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      var uporabniki = [];
      uporabniki.push(vzdevkiGledeNaSocket[socket.id])
      var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
      if (uporabnikiNaKanalu.length > 1) {
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          if (uporabnikSocketId != socket.id) {
            uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
          }
        }
        
      }
      socket.emit('uporabniki', uporabniki);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function obdelajZasebnaSporocila(socket, vzdevkiGledeNaSocket){
  socket.on('zasebnoSporocilo', function(podatki){
    for(var user in vzdevkiGledeNaSocket){
      if(vzdevkiGledeNaSocket[user]==podatki.uporabnik && vzdevkiGledeNaSocket[user] != vzdevkiGledeNaSocket[socket.id]){
        io.sockets.socket(user).emit('sporocilo', {besedilo: "Zasebno sporočilo od "+vzdevkiGledeNaSocket[socket.id]+": "+podatki.sporocilo});
        return;
      }
    }
    socket.emit('sporocilo', {besedilo:"Sporočilo "+ podatki.sporocilo +" uporabniku z vzdevkom "+podatki.uporabnik+" ni bilo mogoče posredovati."})
    
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

 //Popravek za prikaz vzdevka prijavljenega uporabnika
function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek:vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.', sistemsko:true
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek, sistemsko:true});
  }
}

function pridruzitevKanaluZGeslom(socket, kanal, geslo){
  if(geslaZaKanale[kanal]){
    if(geslaZaKanale[kanal] == geslo){
      socket.leave(trenutniKanal[socket.id]);
      if(!io.sockets.manager.rooms["/"+trenutniKanal[socket.id]] && geslaZaKanale[trenutniKanal[socket.id]]){
        delete geslaZaKanale[trenutniKanal[socket.id]];
      }
      socket.join(kanal);
      trenutniKanal[socket.id] = kanal;
      
      socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek:vzdevkiGledeNaSocket[socket.id]});
      socket.broadcast.to(kanal).emit('sporocilo', {
        besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
      });
      
      var uporabnikiNaKanalu = io.sockets.clients(kanal);
      if (uporabnikiNaKanalu.length > 1) {
        var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          if (uporabnikSocketId != socket.id) {
            if (i > 0) {
              uporabnikiNaKanaluPovzetek += ', ';
            }
            uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
          }
        }
        uporabnikiNaKanaluPovzetek += '.';
        socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
      }
    }else{
      socket.emit('sporocilo', {besedilo:"Pridružitev v kanal "+kanal+" ni bilo uspešno, ker je geslo napačno!"});
    }
  }else{
    if(io.sockets.manager.rooms["/"+kanal]){
      socket.emit('sporocilo', {besedilo: "Izbrani kanal "+kanal+" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+kanal+" ali zahtevajte kreiranje kanala z drugim imenom."});
    }else{
      geslaZaKanale[kanal] = geslo;
      socket.leave(trenutniKanal[socket.id]);
      socket.join(kanal);
      trenutniKanal[socket.id] = kanal;
      socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek:vzdevkiGledeNaSocket[socket.id]});
      
      var uporabnikiNaKanalu = io.sockets.clients(kanal);
      if (uporabnikiNaKanalu.length > 1) {
        var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          if (uporabnikSocketId != socket.id) {
            if (i > 0) {
              uporabnikiNaKanaluPovzetek += ', ';
            }
            uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
          }
        }
        uporabnikiNaKanaluPovzetek += '.';
        socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
      }
    }
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.', sistemsko:true
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: sporocilo.besedilo, avtor: vzdevkiGledeNaSocket[socket.id] + ': '

    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if(kanal.geslo){
      pridruzitevKanaluZGeslom(socket, kanal.novKanal, kanal.geslo);
    }else{
      if(geslaZaKanale[kanal.novKanal]){
        socket.emit('sporocilo', {besedilo: "Pridružitev v kanal "+kanal.novKanal+" ni bilo uspešno, ker je geslo napačno!"});
      }else{
        socket.leave(trenutniKanal[socket.id]);
        if(!io.sockets.manager.rooms["/"+trenutniKanal[socket.id]] && geslaZaKanale[trenutniKanal[socket.id]]){
          delete geslaZaKanale[trenutniKanal[socket.id]];
        }
        pridruzitevKanalu(socket, kanal.novKanal);
      }
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    if(io.sockets.manager.rooms["/"+kanal] && geslaZaKanale[trenutniKanal[socket.id]]){
      delete geslaZaKanale[trenutniKanal[socket.id]];
    }
  });
}